﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace TP1
{
    class Program
    {

        static void Main(string[] args)
        {
            exo5();
        }

        private static int AskUserForParameter()
        {
            Console.WriteLine("Please write a number and press enter :");
            int.TryParse(Console.ReadLine(), out var result);
            return result;
        }

        private static int PowerFunction(int x)
        {
            return (int)(Math.Pow(x, 2) - 4);
        }

        static string loadJson()
        {
            string path = @"C:\Users\theok\OneDrive - Efrei\Master 1\ST2ISA Intégration de systèmes application\TP1\DOGE_AllDataPoints_3Days.json";
            string readText = File.ReadAllText(path);
            return readText;
        }

        static void exo1()
        {
            int input = AskUserForParameter();
            if (input <= 1000 && input >= 0)
            {
                Console.WriteLine("Table of " + input);
                for (int j = 1; j <= 10; j++)
                {
                    if ((input * j) % 2 == 1)
                    {
                        Console.WriteLine(input + "*" + j + "=" + input * j);
                    }
                }
            }
            else
            {
                Console.WriteLine("Nombre non valide");
            }
        }

        static void exo2_1()
        {
            for (int i = 1; i <= 1000; i++)
            {
                bool isPrime = true;
                for (int j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        isPrime = false;
                    }
                }
                if (isPrime == true)
                {
                    Console.WriteLine(i);
                }
            }
        }

        static void exo2_2()
        {
            int input = AskUserForParameter();
            if (input <= 1000 && input >= 0)
            {
                int fn = 1;
                int fn1 = 1;
                int fn2 = 0;
                for (int i = 2; i < input; i++)
                {
                    fn2 = fn1;
                    fn1 = fn;
                    fn = fn1 + fn2;
                }
                Console.WriteLine(fn);
            }
            else
            {
                Console.WriteLine("Nombre invalide");
            }
        }

        static void exo2_3()
        {
            int input = AskUserForParameter();
            if (input <= 25 && input >= 0)
            {
                int value = input;
                for (int i = input - 1; i > 0; i--)
                {
                    value = value * i;
                }
                Console.WriteLine(value);
            }
            else
            {
                Console.WriteLine("Nombre invalide");
            }
        }

        static void exo3()
        {
            for (int i = -3; i <= 3; i++)
            {
                int value = PowerFunction(i);
                try
                {
                    Console.WriteLine("10 / " + value + " => " + 10 / value);
                }
                catch
                {
                    Console.WriteLine("Impossible de faire: 10/" + value);
                }
            }
        }

        static void exo4()
        {
            // int.TryParse(Console.ReadLine(), out var result);
            Console.WriteLine("Entre deux nombre (x y):");
            string input = Console.ReadLine();
            string[] inputArray = input.Split(" ");
            int h = Int32.Parse(inputArray[0]);
            int l = Int32.Parse(inputArray[1]);
            if (h >= 1 && h <= 1000 && l >= 1 && l <= 1000)
            {
                // h = h + 2;
                // l = l + 2;
                for (int i = 1; i <= h; i++)
                {
                    for (int j = 1; j <= l; j++)
                    {
                        if (i == 1 && j == 1 || i == h && j == 1 || i == 1 && j == l || i == h && j == l)
                        {
                            Console.Write("0");
                        }
                        else if (i == 1 || i == h)
                        {
                            Console.Write("_");
                        }
                        else if (j == 1 || j == l)
                        {
                            Console.Write("|");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                        if (j == l)
                        {
                            Console.Write("\n");
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Nombre invalide");
            }
        }

        static void exo5()
        {
            string jsonString = loadJson();
            dynamic json = JsonConvert.DeserializeObject(jsonString);
            Console.WriteLine(json.data[0].name);
        }

    }
}